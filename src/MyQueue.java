import java.util.ArrayList;

public class MyQueue<T> {
    private ArrayList<T> queue = new ArrayList();

    public void pushBack(T element) {
        queue.add(queue.size(), element);
    }

    public T popFront() {
        if (!queue.isEmpty()) {
            T tmp = queue.get(0);
            queue.remove(0);
            return tmp;
        } else throw new EmptyArrayException();
    }

    public T peekFront() {
        if (!queue.isEmpty())
            return queue.get(0);
        else throw new EmptyArrayException();
    }

    public T peekBack() {
        if (!queue.isEmpty())
            return queue.get(queue.size() - 1);
        else throw new EmptyArrayException();
    }

    public int size() {
        return queue.size();
    }

    public void pushFront(T element) {
        queue.add(0, element);
    }

    public T popBack() {
        T tmp = queue.get(queue.size() - 1);
        queue.remove(queue.size() - 1);
        return tmp;
    }
}
