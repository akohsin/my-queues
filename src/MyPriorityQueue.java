import java.util.ArrayList;

public class MyPriorityQueue<T> {
    private ArrayList<QueueElement<T>> priorityQueue = new ArrayList<>();
    private boolean order;

    public MyPriorityQueue(boolean ascendingOrder) {
        this.order = order;
    }


    public void push(T element, int priority) {
        for (int i = 0; i < priorityQueue.size(); i++) {
            if (order) {
                if (priorityQueue.get(i).getPriority() > priority) {
                    priorityQueue.add(i, new QueueElement<T>(element, priority));
                    break;
                }
            } else {
                if (priorityQueue.get(i).getPriority() < priority) {
                    priorityQueue.add(i, new QueueElement<T>(element, priority));
                    break;

                }
            }
        }
    }

    public T pop() {
        if (!priorityQueue.isEmpty()) {
            return priorityQueue.get(0).getElement();
        } else throw new EmptyArrayException();
    }

    public int size() {
        return priorityQueue.size();
    }

    public void print() {
        for (QueueElement q : priorityQueue) {
            System.out.print(q.getElement()+", ");
        }
    }


    class QueueElement<T> {
        T element;
        int priority;

        public QueueElement(T element, int priority) {
            this.element = element;
            this.priority = priority;
        }

        public T getElement() {
            return element;
        }

        public int getPriority() {
            return priority;
        }
    }
}
